module gitlab.com/kd-org/golib/clone

go 1.20

require (
	github.com/jinzhu/copier v0.3.5
	github.com/stretchr/testify v1.8.3
	gitlab.com/kd-org/golib/native v0.0.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
