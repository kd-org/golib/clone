package clone

import (
	"testing"

	"github.com/stretchr/testify/require"
	. "gitlab.com/kd-org/golib/native"
)

type Data struct {
	Type  string
	Text  string
	TextP *string
}
type Input struct {
	Text  string
	TextP *string
}

func TestCopy(t *testing.T) {
	t.Parallel()

	input := Input{"foo", Pointer("pfoo")}
	cloned, err := Copy(&Data{Type: "type"}, &input)
	require.NoError(t, err)
	require.EqualValues(t, cloned.Type, "type")
	require.EqualValues(t, cloned.Text, input.Text)
	require.EqualValues(t, *cloned.TextP, *input.TextP)
	input.Text = input.Text + "X"
	*input.TextP = *input.TextP + "X"
	require.NotEqualValues(t, cloned.Text, input.Text)
	require.NotEqualValues(t, *cloned.TextP, *input.TextP)
}

func TestClonePointer(t *testing.T) {
	t.Parallel()

	input := Input{"foo", Pointer("pfoo")}
	cloned, err := ClonePointer[Data](&input)
	require.NoError(t, err)
	require.EqualValues(t, cloned.Text, input.Text)
	require.EqualValues(t, *cloned.TextP, *input.TextP)
	input.Text = input.Text + "X"
	*input.TextP = *input.TextP + "X"
	require.NotEqualValues(t, cloned.Text, input.Text)
	require.NotEqualValues(t, *cloned.TextP, *input.TextP)
}

func TestClonePointerSlice(t *testing.T) {
	t.Parallel()

	inputs := []*Input{
		{"foo", Pointer("pfoo")},
	}
	clonedSlice, err := ClonePointerSlice[Data](inputs)
	require.NoError(t, err)
	require.EqualValues(t, clonedSlice[0].Text, inputs[0].Text)
	require.EqualValues(t, *clonedSlice[0].TextP, *inputs[0].TextP)
	inputs[0].Text = inputs[0].Text + "X"
	*inputs[0].TextP = *inputs[0].TextP + "X"
	require.NotEqualValues(t, clonedSlice[0].Text, inputs[0].Text)
	require.NotEqualValues(t, *clonedSlice[0].TextP, *inputs[0].TextP)
}
