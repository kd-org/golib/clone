package clone

import (
	"github.com/jinzhu/copier"
)

func Copy[T any](toValue *T, fromValue any) (*T, error) {
	err := copier.Copy(toValue, fromValue)
	return toValue, err
}

func ClonePointer[T any, I any](value *I) (*T, error) {
	var v T
	if err := copier.Copy(&v, value); err != nil {
		return nil, err
	}
	return &v, nil
}

func ClonePointerSlice[T any, I any](values []*I) ([]*T, error) {
	cloned := make([]*T, 0, len(values))
	for _, input := range values {
		var v T
		if err := copier.Copy(&v, input); err != nil {
			return nil, err
		}
		cloned = append(cloned, &v)
	}
	return cloned, nil
}
